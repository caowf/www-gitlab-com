---
layout: job_page
title: "Recruiter"
---

## Responsibilities

* Work with hiring managers to develop and update job descriptions
* Perform job and task analysis to document job requirements and objectives
* Prepare and post jobs to appropriate advertising methods
* Source and attract candidates
* Screen candidates resumes and job applications
* Assess applicants’ relevant knowledge, skills, soft skills, experience and aptitudes
* Monitor and apply recruiting best practices
* Act as a point of contact and build influential candidate relationships during the selection process
* Work with Marketing to develop the Employer Brand in relation to hiring

## Requirements

* Five years experience as a Recruiter, preferably in a technical company
* Familiarity with HR databases, applicant tracking systems, and CMS
* Excellent communication and interpersonal skills
* Strong decision making skills
* College Degree preferred
* Teamwork Orientation
